package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RoutingApiWithUriTemplateController {

    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @GetMapping(value = "/user", consumes = "application/json")
    public User getUser(@RequestBody @Valid User user) {
        return user;
    }

    @PostMapping(value = "/user", consumes = "application/json")
    public User createUser(@RequestBody @Valid User user) {
        return user;
    }

    @DeleteMapping(value = "/user", consumes = "application/json")
    public String deleteUser(@RequestBody @Valid User user) {
        return "deleted";
    }

    @GetMapping(value = "/users", consumes = "application/json")
    public List<User> getUserList(@RequestBody List<User> users) {
        return users;
    }

    @PostMapping(value = "/users", consumes = "application/json")
    public List<User> createUserList(@RequestBody List<User> users) {
        this.users.addAll(users);
        return users;
    }
}
